import React from 'react';


import Card from 'react-bootstrap/Card';
import Button from 'react-bootstrap/Button';

export default function Course() {
	return (
			<Card className="card-highlight">
				<Card.Body>
					<Card.Title>
						<h2>React JS Course</h2>
					</Card.Title>
					<Card.Text>
						<h6>Description</h6>
						<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
						<h6>Price</h6>
						<p>Php 40000</p>
					</Card.Text>
					<Button variant="primary">Enroll Now!</Button>
				</Card.Body>
			</Card>
		)
}