import React from 'react';


import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import Card from 'react-bootstrap/Card'

export default function Highlights() {
	return (
		<Row>
			<Col xs={12} md={4} >
				<Card className="card-highlight">
					<Card.Body>
						<Card.Title>
							<h2>Learn from Home</h2>
						</Card.Title>
						<Card.Text>
							Lorem Ipsum is simply dummy text of the printing and typesetting industry.
						</Card.Text>
					</Card.Body>
				</Card>
			</Col>
			<Col xs={12} md={4} >
				<Card className="card-highlight">
					<Card.Body>
						<Card.Title>
							<h2>Study Now, Pay Later</h2>
						</Card.Title>
						<Card.Text>
							Lorem Ipsum is simply dummy text of the printing and typesetting industry.
						</Card.Text>
					</Card.Body>
				</Card>
			</Col>
			<Col xs={12} md={4} >
				<Card className="card-highlight">
					<Card.Body>
						<Card.Title>
							<h2>Be Part of Our Community</h2>
						</Card.Title>
						<Card.Text>
							Lorem Ipsum is simply dummy text of the printing and typesetting industry.
						</Card.Text>
					</Card.Body>
				</Card>
			</Col>
		</Row>

		)
}