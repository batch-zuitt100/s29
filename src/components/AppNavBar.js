// base import

import React from 'react';

//Bootstrap components
import NavBar from 'react-bootstrap/NavBar';
import Nav from 'react-bootstrap/Nav';

export default function AppNavBar() {
    //export default allows the function to be used in other files
    return (
        <NavBar bg="light" expand="lg">
			<NavBar.Brand href="#home"> Zuitt Booking </NavBar.Brand>
			<NavBar.Toggle aria-controls="basic-navbar-nav" />
			<NavBar.Collapse id="basic-navbar-nav">
				<Nav className="mr-auto">
					<Nav.Link href="#home">Home</Nav.Link>
					<Nav.Link href="#link">Courses</Nav.Link>
				</Nav>
			</NavBar.Collapse>
		</NavBar>
    )
}