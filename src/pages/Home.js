import React from 'react';

// Bootstrap components
import Container from 'react-bootstrap/Container';


// App Components
import Banner from 'components/Banner';
import Highlights from 'components/Highlights';
import Course from 'components/Course';


export default function Home(){
	return(
			<Container fluid>
				<Banner />
				<Highlights />
				<Course />
			</Container>

		)
}